#include "feature.h"

using namespace std;
using namespace cv;

cv::Mat edge; // detected edge (for global use to save computation)
cv::Mat saliency; // saliency map (for global use to save computation)

void show(const cv::Mat &img)
{
  namedWindow("image");
  imshow("image", img);
  waitKey(0);
}

void checkType(const cv::Mat &img)
{
  switch (img.type()) {
    case CV_8UC1: cout << "type: 8UC1" << endl; break;
    case CV_8UC3: cout << "type: 8UC3" << endl; break;
    case CV_32FC1: cout << "type: 32FC1" << endl; break;
    case CV_32FC3: cout << "type: 32FC3" << endl; break;
    default: cout << "not recognized (see checkType())" << endl; break;
  }
}

void genEdge(const cv::Mat &img)
{
  edge = computeEdge(img);
}

cv::Mat computeEdge(const cv::Mat &img)
{
  // convert to YCbCr space, and use Y to compute edge.
  Mat ycbcr;
  cvtColor(img, ycbcr, CV_BGR2YCrCb, 1);
  vector<Mat> ycbcrChannels(3);
  split(ycbcr, ycbcrChannels);
  Mat gray = ycbcrChannels[0];

  // Edge detection
  Mat edge;
  int edgeLowThres = 128 / 3;
  int edgeHighThres = 128 * 2 / 3;
  Canny(gray, edge, edgeLowThres, edgeHighThres);

  return edge;
}

const cv::Mat& getEdge()
{
  return edge;
}

void genSaliency(const cv::Mat &img)
{
  // can try different saliency (by calling genXXXSaliency())
  saliency = genGMRSaliency (img);
}

const cv::Mat& getSaliency()
{
  return saliency;
}

cv::Mat genFGSaliency(const cv::Mat &img)
{
  // convert from Mat to IplImage
  IplImage srcImg = img;
  IplImage *dstImg = cvCreateImage(cvSize(srcImg.width, srcImg.height), 8, 1);

  // run the saliency
  Saliency *saliency = new Saliency;
  saliency->calcIntensityChannel(&srcImg, dstImg);

  // return the saliency (auto converted from IplImage to Mat)
  return dstImg;
}

cv::Mat genGMRSaliency(const cv::Mat& img)
{
  Mat i = img; // because the library has non-const argument
  GMRsaliency s;
  return s.GetSal(i);
}

double calcMean ( const cv:: Mat & img)
{
  double  mean  = 0 ;
  for (int i = 0; i < img.rows; ++i) {
    for (int j = 0; j < img.cols; ++j) {
      mean = mean +  UAT_2D(img, i, j);  
    }
  }
  mean  = mean / (img.rows * img.cols);
  return mean;

}

double calcVariance ( const cv:: Mat& img )
{
  double  mean  = 0 ;
  for (int i = 0; i < img.rows; ++i) {
    for (int j = 0; j < img.cols; ++j) {
      mean  = mean +  UAT_2D(img, i, j);
    }
  }
  mean = mean / (img.rows * img.cols);

  double variance = 0;
  for (int i = 0; i < img.rows; ++i) {
    for (int j = 0; j < img.cols; ++j) {
       variance  =  variance + pow ( ( mean - UAT_2D(img, i, j) ), 2.0) ;
    }
  }  
  
  variance = variance /  (img.rows * img.cols) ;

  return variance ;

}
feature contrastFeature(const cv::Mat &img)
{
  feature f;

  // RMS contrast
  Scalar s = rmsContrast(img);
  for (int i = 0; i < img.channels(); ++i) {
    f.push_back(s[i]);
  }

  return f;
}

Scalar rmsContrast(const cv::Mat &img)
{
  // result = sqrt((img - mean(img))^2 / MN)
  return rms(img - mean(img));
}

cv::Scalar rms(const cv::Mat &img)
{
  Mat tmp;
  pow(img, 2, tmp); // tmp = img^2
  Scalar result;
  divide(img.total(), sum(tmp), result); // result = img^2 / #pixels
  sqrt(result, result);
  return result;
}

feature blurFeature(const cv::Mat &img)
{
  /* There are various ways to measure blurriness,
   * but we need a reference-free method.
   * Examples include:
   *   1. "average edge width" (the wider the blurrer)
   *   2. Cumulative Probability of Blur Detection (CPBD)
   * Here use the simplest one: edge of pyramid
   */
  feature f;
  /* 1. Construct Laplacian pyramid (3 levels) of the image.
   * 2. For each scale of image, apply the same Canny edge detector.
   *    This produces a Laplacian pyramid of edge.
   *    Edge in higher level of pyramid represents wider edge in original image.
   * 3. Use #pixel-is-edge of each pyramid level as feature.
   */
  Mat downsampled = img;
  for (int level = 0; level < 3; ++level) {
    // compute edge of this layer of pyramid
    Mat edge = computeEdge(downsampled);
    // compute #pixel that is edge
    int count = 0;
    for (int i = 0; i < edge.rows; ++i) {
      for (int j = 0; j < edge.cols; ++j) {
        if (UAT_2D(edge, i, j) > 128) {
          ++count;
        }
      }
    }
    float ratio = (float)count / edge.total();
    f.push_back(ratio);
    // downsample 1/2
    pyrDown(downsampled, downsampled);
  }
  return f;
}

feature bgComplexityFeature(const cv::Mat &img)
{
  // Background is more complex if it has larger ratio of 'edge' pixels.
  const Mat &edge = getEdge();
  const Mat &saliency = getSaliency();

  // Compute ratio of pixels that are 'edge'.
  // Count the saliency under each range (0.1, 0.2, ..., 1.0)
  vector<int> saliencyCount(10, 0);
  for (int i = 0; i < edge.rows; ++i) {
    for (int j = 0; j < edge.cols; ++j) {
      if (UAT_2D(edge, i, j) > 128) {
        float value = CLAMP( // in range 0~10
            saliencyCount.size() * FAT_2D(saliency, i, j), 0, 10);
        for (int k = 0; k < value; ++k) {
          ++saliencyCount[k];
        }
      }
    }
  }

  feature f;
  for (int i = 0; i < (int)saliencyCount.size(); ++i) {
    double ratio = (double)saliencyCount[i] / edge.total();
    f.push_back(ratio);
  }
  return f;
}
feature colorMeanVarFeature(const cv::Mat & img)
{
  feature f ;
  Scalar mean, stddev; 
  vector<Mat> bgr(3);
  split(img, bgr);
  for (int k = 0; k < 3; ++k) {
    meanStdDev(bgr[k],mean,stddev);
    f.push_back ( mean[0]) ;
    f.push_back ( stddev [0] );
   // f.push_back( calcMean (bgr[k]) );
   // f.push_back( calcVariance (bgr[k]) );
  }
  Mat img_YUV;
  cvtColor(img, img_YUV, CV_BGR2YCrCb);
  vector<Mat> yuv(3);
  split(img_YUV, yuv);

  for (int k = 0; k < 3; ++k) {
    meanStdDev(yuv[k],mean,stddev);
    f.push_back ( mean[0]) ;
    f.push_back ( stddev [0] );
   // f.push_back( calcMean (yuv[k]) );
   // f.push_back( calcVariance (yuv[k]) );
  }


  Mat img_HSV;
  cvtColor(img, img_HSV, CV_BGR2HSV);
  vector<Mat> hsv(3);
  split(img_HSV, hsv);

  for (int k = 0; k < 3; ++k) {
    meanStdDev(hsv[k],mean,stddev);
    f.push_back ( mean[0]) ;
    f.push_back ( stddev [0] );
   // f.push_back( calcMean (hsv[k]) );
   // f.push_back( calcVariance (hsv[k]) );
  }

  return f;
  

}
feature histogramFeature(const cv::Mat &img)
{
  /* all bins or histogram compose the feature */
  // parameters used to compute histogram
  feature f;
  const int histSize = 256; // number of histogram bins
  const float range[] = { 0, 255 }; // set the ranges (for B, G, R)
  const float *histRange = { range };
  Mat hist;

  //************RGB**************
  // separate the image in 3 places (B, G, R)
  vector<Mat> bgr(3);
  split(img, bgr);
  for (int k = 0; k < 3; ++k) {
    calcHist(&bgr[k], 1, 0, Mat(), hist, 1, &histSize, &histRange);
    for (int i = 0; i < histSize; ++i) {
      f.push_back(FAT_1D(hist, i) / img.total());
    }
  }

  //*************YUV*****************
  Mat img_YUV;
  cvtColor(img, img_YUV, CV_BGR2YCrCb);
  vector<Mat> yuv(3);
  split(img_YUV, yuv);
  for (int k = 0; k < 3; ++k) {
    calcHist(&yuv[k], 1, 0, Mat(), hist, 1, &histSize, &histRange);
    for (int i = 0; i < histSize; ++i) {
      f.push_back(FAT_1D(hist, i) / img.total());
    }
  }


  //************HSV*****************
  Mat img_HSV;
  cvtColor(img, img_HSV, CV_BGR2HSV);
  vector<Mat> hsv(3);
  split(img_HSV, hsv);
  for (int k = 0; k < 3; ++k) {
    calcHist(&hsv[k], 1, 0, Mat(), hist, 1, &histSize, &histRange);
    for (int i = 0; i < histSize; ++i) {
      f.push_back(FAT_1D(hist, i) / img.total());
    }
  }

  return f;
}

feature luminanceHistogramFeature(const cv::Mat &img)
{
  feature f;
  const int histSize = 256; // number of histogram bins
  const float range[] = { 0, 255 }; // set the ranges
  const float *histRange = { range };
  Mat hist;

  // convert to YUV
  Mat img_YUV;
  cvtColor(img, img_YUV, CV_BGR2YCrCb);
  vector<Mat> yuv(3);
  split(img_YUV, yuv);

  // compute the histogram
  calcHist(&yuv[0], 1, 0, Mat(), hist, 1, &histSize, &histRange);
  for (int i = 0; i < histSize; ++i) {
    f.push_back(FAT_1D(hist, i) / img.total());
  }
  return f;
}

feature noiseFeature(const cv::Mat &img)
{
  Mat denoised;
  fastNlMeansDenoisingColored(img, denoised);
  Scalar noiseRMS = rms(img - denoised);

  feature f;
  for (int i = 0; i < img.channels(); ++i) {
    f.push_back(noiseRMS[i]);
  }
  return f;
}

feature ruleOfThirdsFeature(const cv::Mat &img)
{
  /* Represented by how much saliency (ratio of pixels) is on the 1/3 grid.
   * Use Gaussian to model the weighting on the 1/3 grid:
   *   1. highest weighting on the grid.
   *   2. with 2 standard deviation at 1/6 away from the grid.
   * The final result is:
   *   feature = saliency_value * grid_weight / #saliency_pixels
   */
  // grid weighting
  feature f;

  Mat gridWeight = Mat::zeros(img.rows, img.cols, CV_32FC1);

  int num_gp = 5;
  float gp[] = { 0.5, 0.75, 1, 2, 3 };

  const int sigma = min(img.rows, img.cols) / 12; // 2 sigma at 1/6
  float sigma_curr;

  Mat saliency = getSaliency();

  for (int gp_index = 0; gp_index < num_gp; gp_index++){
    sigma_curr = gp[gp_index] * sigma;
    for (int i = 0; i < gridWeight.rows; ++i) {
      // Find the r to calculate Gaussian:
      //   1 / (sigma * sqrt(2 * pi)) * exp(-r^2 / (2 * sigma ^2))
      // Consider vertical (di) and horizontal (dj) distance to grid line,
      // then use r = min{di, dj} to compute the Guassian.
      int di = (i < gridWeight.rows / 2)?
          abs(i - gridWeight.rows / 3):
          abs(i - gridWeight.rows * 2 / 3);
      for (int j = 0; j < gridWeight.cols; ++j) {
        int dj = (j < gridWeight.cols / 2)?
            abs(j - gridWeight.cols / 3):
            abs(j - gridWeight.cols * 2 / 3);
        int r = min(di, dj);
        FAT_2D(gridWeight, i, j) =
            1 / (sigma_curr * sqrt(2 * M_PI)) * exp(
            -pow(double(r), double(2)) /
            (2 * pow(double(sigma_curr), double(2))));
      }
    }

    vector<int> saliencyCount(10, 0);
    vector<float> result(10, 0);
    // feature = saliency_value * grid_weight / #saliency_pixels
    for (int i = 0; i < saliency.rows; ++i) {
      for (int j = 0; j < saliency.cols; ++j) {
        float value = CLAMP(
            saliencyCount.size() * FAT_2D(saliency, i, j), 0, 10);
        for (int k = 0; k < value; ++k) {
          result[k] += value * FAT_2D(gridWeight, i, j);
          ++saliencyCount[k];
        }
      }
    }

    for (int i = 0; i < (int)result.size(); ++i) {
      f.push_back(result[i] / saliencyCount[i]);
    }
  }
  return f;
}

feature symmetryFeature(const cv::Mat &img)
{
  /* Calculate top/down and left/right symmetry in the image.
   * For the two half side (e.g. left & right):
   *   value = rms(left_pixel - right_pixel)
   * Higher value indicates poor symmetry.
   */
  feature f;
  f.push_back(leftRightSymmetry(img)); // left/right half
  f.push_back(leftRightSymmetry(img.t())); // top/down half
  return f;
}

float leftRightSymmetry(const cv::Mat &img)
{
  /* Before calculation, allows for small amount of shift,
   * (for 500px image, shift ~10px. So roughly 1/50 of the image size of shift)
   * because it may not be perfectly symmetric.
   * Therefore, iterate over different pixel-shift, then use
   * the minimum convolution of these iterations as the symmtetry value.
   */
  float minValue = FLT_MAX;
  int halfCols = img.cols / 2;
  int shiftRange = img.cols / 50;
  // Shift the right half (-shiftRange ~ +shiftRange),
  // pick the maximum convolution among them.
  for (int shift = -shiftRange; shift < shiftRange; ++shift) {
    float sum = 0;
    // calculate sum((left - right)^2)
    for (int i = 0; i < img.rows; ++i) {
      for (int j = 0; j < halfCols; ++j) {
        // the point to compare (go left/right from the middle)
        int left = halfCols - j;
        int right = halfCols + j + shift;
        if (right < halfCols || right >= img.cols) {
          continue;
        }
        for (int k = 0; k < img.channels(); ++k) {
          // left/right pixel
          int leftPixel = UAT_3D(img, i, left, k);
          int rightPixel = UAT_3D(img, i, right, k);
          if (leftPixel == 0 && rightPixel == 0) {
            continue;
          }
          // (left - right)^2
          float diff =
              (leftPixel - rightPixel) / ((leftPixel + rightPixel) / 2.f);
          sum += pow(diff, 2);
        }
      }
    }
    // rms of (left - right)
    float rms = sqrt(sum / img.total());
    // pick the maximum convolution
    if (rms < minValue) {
      minValue = rms;
    }
  }

  return minValue;
}

feature centroidFeature(const cv::Mat &img)
{
  feature f;

  // Centroid of saliency.
  // Position represented by percentage of the image size.
  // E.g. for 100x30 image that has centroid (20, 15), result is (0.2, 0.5)
  Vec2f saliencyCentroid = computeSaliencyCentroid(img);
  f.push_back(saliencyCentroid[0]);
  f.push_back(saliencyCentroid[1]);

  // Centroid of color of all pixels.
  // Position represented by percentage of the image size.
  // E.g. for 100x30 image that has centroid (20, 15), result is (0.2, 0.5)
  Vec2f colorCentroid = computeColorCentroid(img);
  f.push_back(colorCentroid[0]);
  f.push_back(colorCentroid[1]);

  return f;
}

cv::Vec2f computeSaliencyCentroid(const cv::Mat &img)
{
  const Mat &saliency = getSaliency();
  float sumI = 0, sumJ = 0, sum = 0;
  for (int i = 0; i < saliency.rows; ++i) {
    for (int j = 0; j < saliency.cols; ++j) {
      float v = FAT_2D(saliency, i, j);
      sumI += i * v;
      sumJ += j * v;
      sum += v;
    }
  }
  Vec2f centroid;
  centroid[0] = sumI / sum / img.rows; // the last division is for percentage
  centroid[1] = sumJ / sum / img.cols; // the last division is for percentage
  return centroid;
}

cv::Vec2f computeColorCentroid(const cv::Mat &img)
{
  /* Use Ou's color weight model.
   * (published in "A Study of Colour Emotion and Colour Preference.")
   * in HLS color domain, color weight is given by
   *   weight = -1.8 + 0.04(100-L) + 0.45cos(h-100)
   * (h in degree)
   * Since weight is in the range of -8.45 ~ +2.65,
   * to normalize the weight to positive range we add 8.45 to its value.
   */
  Mat hls;
  cvtColor(img, hls, CV_BGR2HLS);
  float sumI = 0, sumJ = 0, sum = 0;
  for (int i = 0; i < hls.rows; ++i) {
    for (int j = 0; j < hls.cols; ++j) {
      int h = UAT_3D(hls, i, j, 0); // in degree
      int l = UAT_3D(hls, i, j, 1);
      float weight = // 8.45 normalizes its range (-8.45 ~ +2.65) to positive
          8.45 - 1.8 + 0.04 * (100 - l) + 0.45 * cos((h - 100) * M_PI / 180);
      sumI += i * weight;
      sumJ += j * weight;
      sum += weight;
    }
  }

  Vec2f centroid;
  centroid[0] = sumI / sum / hls.rows;
  centroid[1] = sumJ / sum / hls.cols;
  return centroid;
}

feature grayScaleFeature(const cv::Mat &img)
{
  feature f;
  for (int i = 0; i < img.rows; ++i) {
    for (int j = 0; j < img.cols; ++j) {
      if (UAT_3D(img, i, j, 0) != UAT_3D(img, i, j, 1) ||
          UAT_3D(img, i, j, 1) != UAT_3D(img, i, j, 2)) {
        f.push_back(0);
        return f;
      }
    }
  }
  f.push_back(1);
  return f;
}
