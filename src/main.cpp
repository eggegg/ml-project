#include <unistd.h>
#include <iostream>
#include "feature.h"

using namespace std;
using namespace cv;

typedef feature (*featureFunc)(const Mat&);
const string features[] = {
  "bgcomplexity",
  "blur",
  "centroid",
  "contrast",
  "histogram",
  "luminancehistogram",
  "noise",
  "ruleofthirds",
  "symmetry",
  "grayscale",
  "colormeanvariance"
};
const featureFunc funcs[] = {
  &bgComplexityFeature,
  &blurFeature,
  &centroidFeature,
  &contrastFeature,
  &histogramFeature,
  &luminanceHistogramFeature,
  &noiseFeature,
  &ruleOfThirdsFeature,
  &symmetryFeature,
  &grayScaleFeature,
  &colorMeanVarFeature
};
const int featureCount = sizeof(funcs) / sizeof(featureFunc);

int getIndex(string s) {
  for (int i = 0; i < featureCount; ++i) {
    if (s == features[i]) return i;
  }
  return -1;
}

int main(int argc, char **argv)
{
  char c;
  vector<int> exec_order;

  while ((c = getopt(argc, argv, "ahm:")) != -1) {
    switch (c) {
      case 'm': {
        int index = getIndex(optarg);
        if (index == -1) {
          cout << "Unrecognized method." << endl;
          return -1;
        }
        exec_order.push_back(index);
        break;
      }
      case 'a':
        for (int i = 0; i < featureCount; ++i) {
          exec_order.push_back(i);
        }
        break;
      case 'h':
      default:
        cout << "USAGE: " << argv[0] << endl
             << "\t-m [FEATURE]" << endl
             << "\t-a (ALL FEATURE)" << endl
             << "\t[IMAGE FILE]" << endl;
    }
  }
  Mat image;
  if (optind < argc) {
    image = imread(argv[optind], CV_LOAD_IMAGE_COLOR);
    if (image.empty()) {
      cout << "Invalid image. Make sure the file exists." << endl;
      exit(0);
    }
  } else {
    cout << "Need to specify a image." << endl;
    exit(0);
  }

  genEdge(image);
  genSaliency(image);

  feature result;
  for (vector<int>::iterator i = exec_order.begin();
      i < exec_order.end(); ++i) {
    feature single = funcs[*i](image);
    for (feature::iterator j = single.begin(); j != single.end(); ++j) {
      if (j != single.begin()) {
        cout << ",";
      }
      cout << *j;
    }
    cout << endl;
  }
  cout << endl;
  return 0;
}
