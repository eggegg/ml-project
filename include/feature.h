#ifndef __FEATURE_H__
#define __FEATURE_H__

#include <vector>
#include <cmath>
#include <limits>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo/photo.hpp>
#include "saliency.h" // Fine-grained saliency (by Montabone)
#include "GMRsaliency.h" // Graph-based Manifold Ranking (CVPR '13)

#define _USE_MATH_DEFINES

#define CLAMP(n, a, b) (((n) < (a))? (a) : (((n) > (b))? (b) : (n)))
/* Macro for accessing Mat data.
 * The first argument is the Mat itself.
 * Usage: float rgb = FAT_3D(img, 1, 2, 3);
 * Choose UAT (uchar), SAT (short), FAT (float) for different data type.
 * Choose 1D, 2D, 3D according to the dimension of the Mat.
 */
#define UAT_1D(m, i) (*((uchar*)((m).data) + (i)))
#define SAT_1D(m, i) (*((int*)((m).data) + (i)))
#define FAT_1D(m, i) (*((float*)((m).data) + (i)))
#define UAT_2D(m, i, j) (*((uchar*)((m).data) + (i) * (m).cols + (j)))
#define SAT_2D(m, i, j) (*((int*)((m).data) + (i) * (m).cols + (j)))
#define FAT_2D(m, i, j) (*((float*)((m).data) + (i) * (m).cols + (j)))
#define UAT_3D(m, i, j, k) (*((uchar*)((m).data) + \
          (i) * (m).cols * (m).channels() + (j) * (m).channels() + (k)))
#define SAT_3D(m, i, j, k) (*((int*)((m).data) + \
          (i) * (m).cols * (m).channels() + (j) * (m).channels() + (k)))
#define FAT_3D(m, i, j, k) (*((float*)((m).data) + \
          (i) * (m).cols * (m).channels() + (j) * (m).channels() + (k)))

typedef std::vector<double> feature;

/* Utils */
void show(const cv::Mat &img); // imshow the image
void checkType(const cv::Mat &img); // print the matrix type (e.g. CV_8UC3)

/* Edge */
void genEdge(const cv::Mat &img); // generate the global edge
cv::Mat computeEdge(const cv::Mat &img); // generate the detected edge
const cv::Mat& getEdge(); // get the detected edge (global variable)

/* Saliency */
void genSaliency(const cv::Mat &img); // generate the global saliency map
const cv::Mat& getSaliency(); // get the saliency map (global variable)
cv::Mat genFGSaliency(const cv::Mat &img); // Fine-grained saliency (Montabone)
cv::Mat genGMRSaliency(const cv::Mat &img); // GMR saliency (CVPR '13)

/* Contrast */
feature contrastFeature(const cv::Mat &img);
cv::Scalar rmsContrast(const cv::Mat &img);
cv::Scalar rms(const cv::Mat &img); // root-mean-square of all pixels

/* Blurriness */
feature blurFeature(const cv::Mat &img); // measure of foreground blurriness

/* Background complexity */
feature bgComplexityFeature(const cv::Mat &img);

/* RGB histogram */
feature histogramFeature(const cv::Mat &img);

/* Luminance histogram */
feature luminanceHistogramFeature(const cv::Mat &img);

/* Noise */
feature noiseFeature(const cv::Mat &img); // root-mean-square of noise

/* Rule of three */
feature ruleOfThirdsFeature(const cv::Mat &img);

/* Symmetry */
feature symmetryFeature(const cv::Mat &img);
float leftRightSymmetry(const cv::Mat &img);

/* Centroid */
feature centroidFeature(const cv::Mat &img);
cv::Vec2f computeSaliencyCentroid(const cv::Mat &img);
cv::Vec2f computeColorCentroid(const cv::Mat &img);


/*color mean variance */
double calcMean ( const cv:: Mat & img);
double calcVariance ( const cv:: Mat& img );
feature colorMeanVarFeature(const cv::Mat & img);
/* For grayscale image */
feature grayScaleFeature(const cv::Mat &img);

#endif
