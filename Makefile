OPENCV = opencv
#OPENCV = $(HOME)/opencv/lib/pkgconfig/opencv.pc

CFLAGS  = -g -Wall -Iinclude `pkg-config --cflags $(OPENCV)`
LDFLAGS = `pkg-config --libs $(OPENCV)` -lm

CXX     = g++
OBJ     = src/main.o src/feature.o src/saliency.o src/GMRsaliency.o src/SLIC.o
GOAL    = scoring

src/%.o: src/%.cpp
	$(CXX) -c $(CFLAGS) $< -o $@

$(GOAL): $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS)

clean:
	rm src/*.o $(GOAL)
